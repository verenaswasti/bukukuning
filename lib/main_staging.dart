import 'package:bukukuning/app/app.dart';
import 'package:bukukuning/bootstrap.dart';

void main() {
  bootstrap(() => const App());
}
