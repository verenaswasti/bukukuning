part of 'get_contact_list_bloc.dart';

abstract class GetContactListState extends Equatable {
  const GetContactListState();

  @override
  List<Object> get props => [];
}

class GetContactListInitial extends GetContactListState {}

class ContactLoading extends GetContactListState {}

class ContactLoaded extends GetContactListState {
  const ContactLoaded(this.contacts);
  final List<ContactListModel> contacts;

  @override
  List<Object> get props => [contacts];
}

class ContactError extends GetContactListState {
  const ContactError(this.errorMessage);
  final String errorMessage;

  @override
  List<Object> get props => [errorMessage];
}
