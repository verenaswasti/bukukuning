part of 'get_contact_list_bloc.dart';

abstract class GetContactListEvent extends Equatable {
  const GetContactListEvent();

  @override
  List<Object> get props => [];
}

class FetchContacts extends GetContactListEvent {}

class SearchContactByName extends GetContactListEvent {
  const SearchContactByName(this.name);
  final String name;

  @override
  List<Object> get props => [name];
}

class SortByGender extends GetContactListEvent {
  SortByGender(this.isMale);
  final bool isMale;

  @override
  List<Object> get props => [isMale];
}
