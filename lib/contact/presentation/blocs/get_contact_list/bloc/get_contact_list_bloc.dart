import 'package:bloc/bloc.dart';
import 'package:bukukuning/contact/data/datasources/remotes/remote_contact.dart';
import 'package:bukukuning/contact/data/models/contact_list_model.dart';
import 'package:dio/dio.dart';
import 'package:equatable/equatable.dart';

part 'get_contact_list_event.dart';
part 'get_contact_list_state.dart';

class GetContactListBloc
    extends Bloc<GetContactListEvent, GetContactListState> {
  final Dio dio;

  GetContactListBloc(this.dio) : super(GetContactListInitial()) {
    on<FetchContacts>((event, emit) async {
      emit(ContactLoading());
      try {
        RemoteContactImpl remoteContact = RemoteContactImpl(dio);
        List<ContactListModel> contacts = await remoteContact.getContactList();
        emit(ContactLoaded(contacts));
      } catch (error) {
        emit(ContactError(error.toString()));
      }
    });

    on<SearchContactByName>((event, emit) async {
      emit(ContactLoading());
      try {
        RemoteContactImpl remoteContact = RemoteContactImpl(Dio());
        List<ContactListModel> contacts =
            await remoteContact.searchContactByName(event.name);
        emit(ContactLoaded(contacts));
      } catch (error) {
        emit(ContactError(error.toString()));
      }
    });

    on<SortByGender>((event, emit) async {
      emit(ContactLoading());
      try {
        final String baseUrl =
            'https://658143ce3dfdd1b11c42c686.mockapi.io/bukukuning/contacts';
        final String genderEndpoint =
            event.isMale ? '?gender=false' : '?gender=true';
        final String apiUrl = baseUrl + genderEndpoint;

        final response = await dio.get<List<dynamic>>(
          apiUrl,
        );

        if (response.statusCode == 200) {
          final responseData = response.data ?? [];
          final contacts = responseData
              .map(
                (data) =>
                    ContactListModel.fromJson(data as Map<String, dynamic>),
              )
              .toList();
          emit(ContactLoaded(contacts));
        } else {
          throw Exception('Failed to fetch contacts');
        }
      } catch (error) {
        emit(ContactError(error.toString()));
      }
    });
  }
}
