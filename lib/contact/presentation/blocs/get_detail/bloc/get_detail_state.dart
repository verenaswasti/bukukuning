part of 'get_detail_bloc.dart';

sealed class GetDetailState extends Equatable {
  const GetDetailState();

  @override
  List<Object> get props => [];
}

final class GetDetailInitial extends GetDetailState {}

class DetailContactLoading extends GetDetailState {}

class DetailContactLoaded extends GetDetailState {
  const DetailContactLoaded(this.contact);
  final ContactDetailModel contact;

  @override
  List<Object> get props => [contact];
}

class DetailContactError extends GetDetailState {
  const DetailContactError(this.errorMessage);
  final String errorMessage;

  @override
  List<Object> get props => [errorMessage];
}

class ContactDeletedSuccess extends GetDetailState {}

class ContactDeleteFailure extends GetDetailState {
  const ContactDeleteFailure(this.errorMessage);
  final String errorMessage;

  @override
  List<Object> get props => [errorMessage];
}
