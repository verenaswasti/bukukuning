import 'package:bloc/bloc.dart';
import 'package:bukukuning/contact/data/datasources/remotes/remote_contact.dart';
import 'package:bukukuning/contact/data/models/contact_detail_model.dart';
import 'package:dio/dio.dart';
import 'package:equatable/equatable.dart';

part 'get_detail_event.dart';
part 'get_detail_state.dart';

class GetDetailBloc extends Bloc<GetDetailEvent, GetDetailState> {
  GetDetailBloc(this.dio) : super(GetDetailInitial()) {
    on<FetchContactDetail>((event, emit) async {
      emit(DetailContactLoading());
      try {
        final remoteContact = RemoteContactImpl(dio);
        final contact = await remoteContact.getContactDetail(event.id);
        emit(DetailContactLoaded(contact));
      } catch (error) {
        emit(DetailContactError(error.toString()));
      }
    });

    on<DeleteContact>((event, emit) async {
      emit(DetailContactLoading());
      try {
        final remoteContact = RemoteContactImpl(dio);
        await remoteContact.deleteContact(event.id);
        emit(ContactDeletedSuccess());
      } catch (error) {
        emit(ContactDeleteFailure(error.toString()));
      }
    });
  }
  final Dio dio;
}
