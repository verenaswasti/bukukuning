part of 'get_detail_bloc.dart';

sealed class GetDetailEvent extends Equatable {
  const GetDetailEvent();

  @override
  List<Object> get props => [];
}

class FetchContactDetail extends GetDetailEvent {
  const FetchContactDetail(this.id);
  final String id;

  @override
  List<Object> get props => [id];
}

class DeleteContact extends GetDetailEvent {
  const DeleteContact(this.id);
  final String id;

  @override
  List<Object> get props => [id];
}
