import 'package:bukukuning/contact/presentation/blocs/blocs.dart';
import 'package:bukukuning/contact/presentation/pages/contact_detail.dart';
import 'package:bukukuning/contact/presentation/widgets/display_user.dart';
import 'package:bukukuning/contact/presentation/widgets/loading_state.dart';
import 'package:bukukuning/contact/presentation/widgets/search_box.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_phosphor_icons/flutter_phosphor_icons.dart';
import 'package:google_fonts/google_fonts.dart';

class ContactHomePage extends StatefulWidget {
  const ContactHomePage({super.key});

  @override
  State<ContactHomePage> createState() => _ContactHomePageState();
}

class _ContactHomePageState extends State<ContactHomePage> {
  late final GetContactListBloc _contactListBloc;
  TextEditingController searchController = TextEditingController();
  late final FocusNode _searchFocusNode = FocusNode();

  @override
  void initState() {
    super.initState();
    _contactListBloc = GetContactListBloc(Dio());
    _contactListBloc.add(FetchContacts());
    searchController.addListener(onSearchChanged);
  }

  @override
  void dispose() {
    _contactListBloc.close();
    _searchFocusNode.dispose();
    searchController.dispose();
    super.dispose();
  }

  void onSearchChanged() {
    final query = searchController.text;
    if (query.isNotEmpty) {
      _contactListBloc.add(SearchContactByName(query));
    } else {
      _contactListBloc.add(FetchContacts());
    }
  }

  void closeKeyboard() {
    _searchFocusNode.unfocus();
  }

  void showDetailContact(String contactId) {
    Navigator.push(
      context,
      MaterialPageRoute<void>(
        builder: (context) => ContactDetailPage(contactId: contactId),
      ),
    );
  }

  void _sortByGender(BuildContext context, bool isMale) {
    _contactListBloc.add(SortByGender(isMale));
    Navigator.pop(context);
  }

  void _showGenderModal(BuildContext context) {
    showModalBottomSheet<void>(
      context: context,
      builder: (BuildContext context) {
        return SizedBox(
          height: MediaQuery.of(context).size.height * 0.20,
          width: double.infinity,
          child: Padding(
            padding: const EdgeInsets.only(top: 30, left: 20, right: 20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'Sort by Gender',
                      style: GoogleFonts.inter(
                        fontWeight: FontWeight.w600,
                        fontSize: 15,
                      ),
                    ),
                    Text(
                      'Clear Filter',
                      style: GoogleFonts.inter(color: const Color(0xffF39200)),
                    ),
                  ],
                ),
                const SizedBox(
                  height: 20,
                ),
                GestureDetector(
                  onTap: () {
                    // _sortByGender(context, true);
                    // Navigator.pop(context);
                  },
                  child: Text(
                    'Female',
                    style: GoogleFonts.inter(fontSize: 15),
                  ),
                ),
                const Divider(
                  color: Color(0xffE0E0E0),
                  thickness: 1,
                ),
                GestureDetector(
                  onTap: () {
                    // _sortByGender(context, false);
                    // Navigator.pop(context);
                  },
                  child: Text(
                    'Male',
                    style: GoogleFonts.inter(fontSize: 15),
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // floatingActionButton: Padding(
      //   padding: const EdgeInsets.only(right: 20, bottom: 20),
      //   child: FloatingActionButton(
      //     backgroundColor: const Color(0xffFFAA3D),
      //     shape: const CircleBorder(),
      //     onPressed: () {},
      //     child: const Icon(Icons.add),
      //   ),
      // ),
      backgroundColor: Colors.white,
      appBar: AppBar(
        scrolledUnderElevation: 0,
        backgroundColor: Colors.white,
        flexibleSpace: Center(
          child: Transform.translate(
            offset: const Offset(0, 35),
            child: Image.asset('assets/images/logo/logo_home.png'),
          ),
        ),
      ),
      body: MultiBlocProvider(
        providers: [
          BlocProvider.value(
            value: _contactListBloc,
          ),
        ],
        child: Column(
          children: [
            Container(
              margin: const EdgeInsets.only(top: 40, left: 20, right: 20),
              height: 45,
              child: Row(
                children: [
                  SearchBox(
                    focusNode: _searchFocusNode,
                    controller: searchController,
                    onTap: closeKeyboard,
                  ),
                  const SizedBox(width: 12),
                  GestureDetector(
                    onTap: () {
                      _showGenderModal(context);
                    },
                    child: const Icon(
                      PhosphorIcons.sliders_horizontal,
                      size: 32,
                      color: Color(0xff2b2b2b),
                    ),
                  ),
                ],
              ),
            ),
            const SizedBox(height: 28),
            Expanded(
              child: BlocBuilder<GetContactListBloc, GetContactListState>(
                bloc: _contactListBloc,
                builder: (context, state) {
                  if (state is ContactLoading) {
                    return const LoadingState();
                  } else if (state is ContactLoaded) {
                    final contacts = state.contacts;
                    return ListView.builder(
                      itemCount: contacts.length,
                      itemBuilder: (BuildContext context, index) {
                        final contact = contacts[index];
                        return DisplayUser(
                          onTap: () => showDetailContact(contact.id!),
                          avatar: contact.avatar ?? '',
                          name: contact.name ?? '',
                          phoneNumber: contact.phoneNumber ?? '',
                          contactId: contact.id,
                        );
                      },
                    );
                  } else if (state is ContactError) {
                    return Center(
                      child: Text('Error: ${state.errorMessage}'),
                    );
                  } else {
                    return const Center(
                      child: Text('No Data!'),
                    );
                  }
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
