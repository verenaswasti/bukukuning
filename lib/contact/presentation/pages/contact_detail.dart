import 'package:bukukuning/contact/presentation/blocs/blocs.dart';
import 'package:bukukuning/contact/presentation/blocs/get_detail/bloc/get_detail_bloc.dart';
import 'package:bukukuning/contact/presentation/pages/contact_home.dart';
import 'package:bukukuning/contact/presentation/widgets/widgets.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';

class ContactDetailPage extends StatefulWidget {
  const ContactDetailPage({super.key, this.contactId});

  final String? contactId;

  @override
  State<ContactDetailPage> createState() => _ContactDetailPageState();
}

class _ContactDetailPageState extends State<ContactDetailPage> {
  late GetDetailBloc _detailBloc;

  @override
  void initState() {
    super.initState();
    _detailBloc = GetDetailBloc(Dio());

    if (widget.contactId != null) {
      _detailBloc.add(FetchContactDetail(widget.contactId!));
    }
  }

  void _deleteContact() {
    showDialog<void>(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          backgroundColor: Colors.white,
          title: Text(
            'Delete Contact',
            style: GoogleFonts.inter(fontSize: 15, fontWeight: FontWeight.w600),
          ),
          content: Text(
            'Are you sure? Deleted contact cannot be retrieved',
            style: GoogleFonts.inter(fontWeight: FontWeight.w400),
          ),
          actions: [
            TextButton(
              onPressed: () {
                Navigator.of(context).pop();
              },
              child: Text(
                'Go Back',
                style: GoogleFonts.inter(
                  color: Colors.black,
                  fontWeight: FontWeight.w600,
                ),
              ),
            ),
            TextButton(
              onPressed: () {
                Navigator.of(context).pop();
                _detailBloc.add(DeleteContact(widget.contactId!));
              },
              child: Text(
                'Delete',
                style: GoogleFonts.inter(
                  color: const Color(0xFF0E50000),
                  fontWeight: FontWeight.w600,
                ),
              ),
            ),
          ],
        );
      },
    );
  }

  @override
  void dispose() {
    _detailBloc.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider.value(
      value: _detailBloc,
      child: BlocBuilder<GetDetailBloc, GetDetailState>(
        builder: (context, state) {
          if (state is ContactLoading) {
            return Scaffold(
              backgroundColor: Colors.white,
              appBar: AppBar(
                backgroundColor: Colors.white,
                leading: IconButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  icon: const Icon(Icons.arrow_back_ios),
                ),
                title: Text(
                  'Contact Details',
                  style: GoogleFonts.inter(
                    fontSize: 18,
                    fontWeight: FontWeight.w600,
                  ),
                ),
                centerTitle: true,
              ),
              body: const LoadingState(),
            );
          } else if (state is DetailContactLoaded) {
            final contact = state.contact;
            return Scaffold(
              backgroundColor: Colors.white,
              appBar: AppBar(
                backgroundColor: Colors.white,
                leading: IconButton(
                  onPressed: () => Navigator.of(context).pop(),
                  icon: const Icon(Icons.arrow_back_ios),
                ),
                title: Text(
                  'Contact Details',
                  style: GoogleFonts.inter(
                    fontSize: 18,
                    fontWeight: FontWeight.w600,
                  ),
                ),
                centerTitle: true,
              ),
              body: Column(
                children: [
                  const SizedBox(height: 35),
                  CircleAva(value: contact.avatar ?? ''),
                  const SizedBox(height: 20),
                  DisplayName(value: contact.name ?? ''),
                  const SizedBox(height: 20),
                  PhoneRow(value: contact.phoneNumber ?? ''),
                  const SizedBox(height: 20),
                  EmailRow(value: contact.email ?? ''),
                  const SizedBox(height: 20),
                  GenderRow(value: contact.gender ?? ''),
                  const SizedBox(height: 40),
                  GestureDetector(
                    onTap: _deleteContact,
                    child: const ButtonWg(),
                  ),
                ],
              ),
            );
          } else if (state is DetailContactError) {
            return Scaffold(
              body: Center(
                child: Text('Error: ${state.errorMessage}'),
              ),
            );
          } else if (state is ContactDeletedSuccess) {
            WidgetsBinding.instance.addPostFrameCallback((_) {
              Navigator.pushAndRemoveUntil(
                context,
                MaterialPageRoute<void>(
                  builder: (context) => const ContactHomePage(),
                ),
                (route) => false,
              );
            });
            return const LoadingState();
          } else {
            return const LoadingState();
          }
        },
      ),
    );
  }
}
