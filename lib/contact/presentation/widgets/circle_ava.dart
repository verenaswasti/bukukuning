import 'package:flutter/material.dart';

class CircleAva extends StatelessWidget {
  const CircleAva({required this.value, super.key});

  final String value;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: CircleAvatar(
        radius: 60,
        backgroundImage: NetworkImage(
          value,
        ),
      ),
    );
  }
}
