import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class ButtonWg extends StatelessWidget {
  const ButtonWg({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(
        left: MediaQuery.of(context).size.width * 0.12,
        right: MediaQuery.of(context).size.width * 0.12,
      ),
      height: 40,
      decoration: BoxDecoration(
        color: const Color(0xffE50000),
        borderRadius: BorderRadius.circular(16),
      ),
      child: Center(
        child: Text(
          'Delete This Contact',
          style: GoogleFonts.inter(
            color: Colors.white,
            fontSize: 16,
            fontWeight: FontWeight.w600,
          ),
        ),
      ),
    );
  }
}
