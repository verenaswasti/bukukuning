import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class DisplayName extends StatelessWidget {
  const DisplayName({required this.value, super.key});

  final String value;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Text(
        value,
        style: GoogleFonts.inter(
          fontSize: 16,
          fontWeight: FontWeight.w500,
        ),
      ),
    );
  }
}
