import 'package:flutter/material.dart';
import 'package:flutter_phosphor_icons/flutter_phosphor_icons.dart';
import 'package:google_fonts/google_fonts.dart';

// Widget search box
class SearchBox extends StatelessWidget {
  const SearchBox({
    required this.controller,
    required this.focusNode,
    this.onTap,
    super.key,
  });

  final TextEditingController controller;
  final FocusNode focusNode;
  final VoidCallback? onTap;

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Container(
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(16),
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.5),
              spreadRadius: 1,
              blurRadius: 4,
              offset: const Offset(0, 3),
            ),
          ],
        ),
        child: GestureDetector(
          onTap: onTap,
          child: TextField(
            controller: controller,
            focusNode: focusNode,
            // onChanged: onTextChanged,
            decoration: InputDecoration(
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(16),
                borderSide: BorderSide.none,
              ),
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(16),
                borderSide: BorderSide.none,
              ),
              hintText: 'Search by name',
              hintStyle: GoogleFonts.inter(
                color: const Color(0xffBDBDBD),
                fontWeight: FontWeight.w600,
                fontSize: 15,
              ),
              prefixIcon: const Icon(
                PhosphorIcons.magnifying_glass,
                size: 24,
                color: Color(0xffBDBDBD),
              ),
              contentPadding: const EdgeInsets.symmetric(vertical: 10),
            ),
          ),
        ),
      ),
    );
  }
}
