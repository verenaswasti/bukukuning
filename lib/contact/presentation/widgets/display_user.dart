import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

// Widget user yang ditampilkan di list
class DisplayUser extends StatelessWidget {
  const DisplayUser({
    required this.onTap,
    required this.avatar,
    required this.name,
    required this.phoneNumber,
    this.contactId,
    Key? key,
  }) : super(key: key);

  final VoidCallback onTap;
  final String avatar;
  final String name;
  final String phoneNumber;
  final String? contactId;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Column(
        children: [
          Row(
            children: [
              Container(
                margin: const EdgeInsets.only(left: 20, top: 12),
                child: CircleAvatar(
                  radius: 25,
                  backgroundImage: NetworkImage(
                    avatar,
                  ),
                ),
              ),
              Container(
                margin: const EdgeInsets.only(left: 15, top: 12),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      name,
                      style: GoogleFonts.inter(
                        fontWeight: FontWeight.w500,
                        fontSize: 15,
                      ),
                    ),
                    Text(
                      phoneNumber,
                      style: GoogleFonts.inter(
                        fontWeight: FontWeight.w500,
                        fontSize: 14,
                        color: const Color(0xffBDBDBD),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
          const Divider(
            color: Color(0xffE0E0E0),
            thickness: 1,
            indent: 20,
            endIndent: 20,
          ),
        ],
      ),
    );
  }
}
