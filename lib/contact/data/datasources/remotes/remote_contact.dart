import 'dart:developer';

import 'package:bukukuning/contact/data/models/contact_detail_model.dart';
import 'package:bukukuning/contact/data/models/contact_list_model.dart';
import 'package:dio/dio.dart';

mixin RemoteContact {
  Future<List<ContactListModel>> getContactList();
  Future<List<ContactListModel>> searchContactByName(String name);
  Future<ContactDetailModel> getContactDetail(String id);
  Future<void> deleteContact(String id);
  // Future<List<ContactListModel>> getContactByGender(bool gender);
}

class RemoteContactImpl implements RemoteContact {
  RemoteContactImpl(this.dio);
  final Dio dio;

  @override
  Future<List<ContactListModel>> getContactList() async {
    try {
      final response = await dio.get<List<dynamic>>(
        'https://658143ce3dfdd1b11c42c686.mockapi.io/bukukuning/contacts',
      );
      if (response.statusCode == 200) {
        final responseData = response.data ?? [];
        final contacts = responseData
            .map(
              (data) => ContactListModel.fromJson(data as Map<String, dynamic>),
            )
            .toList();
        return contacts;
      } else {
        throw Exception('Failed to fetch contacts');
      }
    } catch (e) {
      rethrow;
    }
  }

  @override
  Future<List<ContactListModel>> searchContactByName(String name) async {
    try {
      final response = await dio.get<List<dynamic>>(
        'https://658143ce3dfdd1b11c42c686.mockapi.io/bukukuning/contacts?name=$name',
      );
      if (response.statusCode == 200) {
        final responseData = response.data ?? [];
        final contacts = responseData
            .map(
              (data) => ContactListModel.fromJson(data as Map<String, dynamic>),
            )
            .toList();
        return contacts;
      } else {
        throw Exception('Failed to fetch contacts');
      }
    } catch (e) {
      rethrow;
    }
  }

  @override
  Future<List<ContactListModel>> getContactByGender(bool isFemale) async {
    try {
      final String baseUrl =
          'https://658143ce3dfdd1b11c42c686.mockapi.io/bukukuning/contacts';
      final String genderEndpoint = isFemale ? '?gender=true' : '?gender=false';
      final String apiUrl = baseUrl + genderEndpoint;

      final response = await dio.get<List<dynamic>>(
        apiUrl,
      );

      if (response.statusCode == 200) {
        final responseData = response.data ?? [];
        final contacts = responseData
            .map(
              (data) => ContactListModel.fromJson(data as Map<String, dynamic>),
            )
            .toList();
        return contacts;
      } else {
        throw Exception('Failed to fetch contacts');
      }
    } catch (e) {
      rethrow;
    }
  }

  @override
  Future<ContactDetailModel> getContactDetail(String id) async {
    try {
      final response = await Dio().get<Map<String, dynamic>>(
        'https://658143ce3dfdd1b11c42c686.mockapi.io/bukukuning/contacts/$id',
      );

      if (response.statusCode == 200) {
        final responseData = response.data;
        final contact = ContactDetailModel.fromJson(responseData!);
        return contact;
      } else {
        throw Exception('Failed to fetch contact');
      }
    } catch (e) {
      rethrow;
    }
  }

  @override
  Future<void> deleteContact(String id) async {
    try {
      final response = await dio.delete(
        'https://658143ce3dfdd1b11c42c686.mockapi.io/bukukuning/contacts/$id',
      );

      if (response.statusCode == 200 || response.statusCode == 204) {
        // Berhasil menghapus kontak
        return;
      } else {
        throw Exception('Failed to delete contact');
      }
    } catch (e) {
      rethrow;
    }
  }
}
