// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'contact_detail_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ContactDetailModel _$ContactDetailModelFromJson(Map<String, dynamic> json) =>
    ContactDetailModel(
      json['name'] as String?,
      json['avatar'] as String?,
      json['phoneNumber'] as String?,
      json['email'] as String?,
      json['gender'] as String?,
      json['id'] as String?,
    );

Map<String, dynamic> _$ContactDetailModelToJson(ContactDetailModel instance) =>
    <String, dynamic>{
      'name': instance.name,
      'avatar': instance.avatar,
      'phoneNumber': instance.phoneNumber,
      'email': instance.email,
      'gender': instance.gender,
      'id': instance.id,
    };
