import 'package:bukukuning/contact/data/entities/contact_list_data.dart';
import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';

part 'contact_detail_model.g.dart';

@JsonSerializable()
class ContactDetailModel extends Equatable {
  const ContactDetailModel(
    this.name,
    this.avatar,
    this.phoneNumber,
    this.email,
    this.gender,
    this.id,
  );

  factory ContactDetailModel.fromJson(Map<String, dynamic> json) =>
      _$ContactDetailModelFromJson(json);

  Map<String, dynamic> toJson() => _$ContactDetailModelToJson(this);

  @JsonKey(name: 'name')
  final String? name;
  @JsonKey(name: 'avatar')
  final String? avatar;
  @JsonKey(name: 'phoneNumber')
  final String? phoneNumber;
  @JsonKey(name: 'email')
  final String? email;
  @JsonKey(name: 'gender')
  final String? gender;
  @JsonKey(name: 'id')
  final String? id;

  ContactListData toEntity() => ContactListData(
        name ?? '',
        avatar ?? '',
        phoneNumber ?? '',
        email ?? '',
        gender ?? '',
        id ?? '',
      );

  @override
  List<Object?> get props => [name, avatar, phoneNumber, email, gender, id];
}
