import 'package:bukukuning/contact/data/entities/contact_list_data.dart';
import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';

part 'contact_list_model.g.dart';

@JsonSerializable()
class ContactListModel extends Equatable {
  const ContactListModel(
    this.name,
    this.avatar,
    this.phoneNumber,
    this.email,
    this.gender,
    this.id,
  );

  factory ContactListModel.fromJson(Map<String, dynamic> json) =>
      _$ContactListModelFromJson(json);

  Map<String, dynamic> toJson() => _$ContactListModelToJson(this);

  @JsonKey(name: 'name')
  final String? name;
  @JsonKey(name: 'avatar')
  final String? avatar;
  @JsonKey(name: 'phoneNumber')
  final String? phoneNumber;
  @JsonKey(name: 'email')
  final String? email;
  @JsonKey(name: 'gender')
  final String? gender;
  @JsonKey(name: 'id')
  final String? id;

  ContactListData toEntity() => ContactListData(
        name ?? '',
        avatar ?? '',
        phoneNumber ?? '',
        email ?? '',
        gender ?? '',
        id ?? '',
      );

  @override
  List<Object?> get props => [name, avatar, phoneNumber, email, gender, id];
}
