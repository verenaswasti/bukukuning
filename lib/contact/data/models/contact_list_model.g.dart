// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'contact_list_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ContactListModel _$ContactListModelFromJson(Map<String, dynamic> json) =>
    ContactListModel(
      json['name'] as String?,
      json['avatar'] as String?,
      json['phoneNumber'] as String?,
      json['email'] as String?,
      json['gender'] as String?,
      json['id'] as String?,
    );

Map<String, dynamic> _$ContactListModelToJson(ContactListModel instance) =>
    <String, dynamic>{
      'name': instance.name,
      'avatar': instance.avatar,
      'phoneNumber': instance.phoneNumber,
      'email': instance.email,
      'gender': instance.gender,
      'id': instance.id,
    };
