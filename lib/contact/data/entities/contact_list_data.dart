import 'package:equatable/equatable.dart';

class ContactListData extends Equatable {
  const ContactListData(
    this.name,
    this.avatar,
    this.phoneNumber,
    this.email,
    this.gender,
    this.id,
  );

  final String name;
  final String avatar;
  final String phoneNumber;
  final String email;
  final String gender;
  final String id;

  @override
  List<Object?> get props => [name, avatar, phoneNumber, email, gender, id];
}
