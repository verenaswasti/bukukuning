import 'package:bukukuning/contact/presentation/blocs/get_contact_list/bloc/get_contact_list_bloc.dart';
import 'package:bukukuning/contact/presentation/pages/contact_home.dart';
import 'package:bukukuning/l10n/l10n.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class App extends StatelessWidget {
  const App({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => GetContactListBloc(Dio()),
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          useMaterial3: true,
        ),
        localizationsDelegates: AppLocalizations.localizationsDelegates,
        supportedLocales: AppLocalizations.supportedLocales,
        home: const ContactHomePage(),
      ),
    );
  }
}
